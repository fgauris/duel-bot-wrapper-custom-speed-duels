﻿namespace OmegaBotWrapper.Enum
{
    /// <summary>
    /// Actions that can be taken during main phase
    /// </summary>
    public enum MainAction
    {
        Summon = 0,
        SpSummon = 1,
        Repos = 2,
        SetMonster = 3,
        SetSpell = 4,
        Activate = 5,
        ToBattlePhase = 6,
        ToEndPhase = 7,
        ShuffleHand = 8
    }
}