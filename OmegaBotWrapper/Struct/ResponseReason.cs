﻿namespace OmegaBotWrapper.Struct
{
    /// <summary>
    /// General structure of responses
    /// </summary>
    public struct ResponseReason
    {
        /// <summary>
        /// Response code
        /// </summary>
        public int Code;
        /// <summary>
        /// Response reason
        /// </summary>
        public string Reason;
    }
}
