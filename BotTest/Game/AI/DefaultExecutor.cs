﻿using BotTest.Game.Helpers.Enums;
using BotTest.Game.Helpers;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
using BotTest.Game.CardActions.Skill.SkillActions;
using BotTest.Game.CardActions.SpellActions.Enums;
using BotTest.Game.CardActions.Trap.TrapActions;
using BotTest.Game.CardActions.Spell.Enums;
using BotTest.Game.CardActions.Trap.Enums;
using BotTest.Game.CardActions.Monsters.Effects.Enums;
using System;
using System.Reflection;
#pragma warning disable CS8602
#pragma warning disable CS8604
#pragma warning disable CA1822

namespace BotTest.Game.AI
{
    public class DefaultExecutor
    {
        public DuelBotWrapper? BotWrapper { get; set; }

        public void GetDuelBotWrapper(DuelBotWrapper bot)
        {
            this.BotWrapper = bot;
        }

        private bool IsBotCardDefault = false;

        private readonly SpellActions SpellExecutor = new();
        private readonly SkillActions SkillExector = new();
        private int CardID = 0;

        protected class CardId
        {
            public const int JizukirutheStarDestroyingKaiju = 63941210;
            public const int ThunderKingtheLightningstrikeKaiju = 48770333;
            public const int DogorantheMadFlameKaiju = 93332803;
            public const int RadiantheMultidimensionalKaiju = 28674152;
            public const int GadarlatheMysteryDustKaiju = 36956512;
            public const int KumongoustheStickyStringKaiju = 29726552;
            public const int GamecieltheSeaTurtleKaiju = 55063751;
            public const int SuperAntiKaijuWarMachineMechaDogoran = 84769941;

            public const int SandaionTheTimelord = 33015627;
            public const int GabrionTheTimelord = 6616912;
            public const int MichionTheTimelord = 7733560;
            public const int ZaphionTheTimelord = 28929131;
            public const int HailonTheTimelord = 34137269;
            public const int RaphionTheTimelord = 60222213;
            public const int SadionTheTimelord = 65314286;
            public const int MetaionTheTimelord = 74530899;
            public const int KamionTheTimelord = 91712985;
            public const int LazionTheTimelord = 92435533;

            public const int LeftArmofTheForbiddenOne = 7902349;
            public const int RightLegofTheForbiddenOne = 8124921;
            public const int LeftLegofTheForbiddenOne = 44519536;
            public const int RightArmofTheForbiddenOne = 70903634;
            public const int ExodiaTheForbiddenOne = 33396948;

            public const int UltimateConductorTytanno = 18940556;
            public const int ElShaddollConstruct = 20366274;
            public const int AllyOfJusticeCatastor = 26593852;

            public const int DupeFrog = 46239604;
            public const int MaraudingCaptain = 2460565;

            public const int BlackRoseDragon = 73580471;
            public const int JudgmentDragon = 57774843;
            public const int TopologicTrisbaena = 72529749;
            public const int EvilswarmExcitonKnight = 46772449;
            public const int HarpiesFeatherDuster = 18144506;
            public const int DarkMagicAttack = 2314238;
            public const int MysticalSpaceTyphoon = 5318639;
            public const int CosmicCyclone = 8267140;
            public const int GalaxyCyclone = 5133471;
            public const int BookOfMoon = 14087893;
            public const int CompulsoryEvacuationDevice = 94192409;
            public const int CallOfTheHaunted = 97077563;
            public const int Scapegoat = 73915051;
            public const int BreakthroughSkill = 78474168;
            public const int SolemnJudgment = 41420027;
            public const int SolemnWarning = 84749824;
            public const int SolemnStrike = 40605147;
            public const int TorrentialTribute = 53582587;
            public const int EvenlyMatched = 15693423;
            public const int HeavyStorm = 19613556;
            public const int HammerShot = 26412047;
            public const int DarkHole = 53129443;
            public const int Raigeki = 12580477;
            public const int SmashingGround = 97169186;
            public const int PotOfDesires = 35261759;
            public const int AllureofDarkness = 1475311;
            public const int DimensionalBarrier = 83326048;
            public const int InterruptedKaijuSlumber = 99330325;

            public const int ChickenGame = 67616300;
            public const int SantaClaws = 46565218;

            public const int CastelTheSkyblasterMusketeer = 82633039;
            public const int CrystalWingSynchroDragon = 50954680;
            public const int NumberS39UtopiaTheLightning = 56832966;
            public const int Number39Utopia = 84013237;
            public const int UltimayaTzolkin = 1686814;
            public const int MekkKnightCrusadiaAstram = 21887175;
            public const int HamonLordofStrikingThunder = 32491822;

            public const int MoonMirrorShield = 19508728;
            public const int PhantomKnightsFogBlade = 25542642;

            public const int VampireFraeulein = 6039967;
            public const int InjectionFairyLily = 79575620;

            public const int BlueEyesChaosMAXDragon = 55410871;

            public const int AshBlossom = 14558127;
            public const int MaxxC = 23434538;
            public const int LockBird = 94145021;
            public const int GhostOgreAndSnowRabbit = 59438930;
            public const int GhostBelle = 73642296;
            public const int EffectVeiler = 97268402;
            public const int ArtifactLancea = 34267821;

            public const int CalledByTheGrave = 24224830;
            public const int InfiniteImpermanence = 10045474;
            public const int GalaxySoldier = 46659709;
            public const int MacroCosmos = 30241314;
            public const int UpstartGoblin = 70368879;
            public const int CyberEmergency = 60600126;

            public const int EaterOfMillions = 63845230;

            public const int InvokedPurgatrio = 12307878;
            public const int ChaosAncientGearGiant = 51788412;
            public const int UltimateAncientGearGolem = 12652643;

            public const int RedDragonArchfiend = 70902743;

            public const int ImperialOrder = 61740673;
            public const int RoyalDecreel = 51452091;
            public const int NaturiaBeast = 33198837;
            public const int AntiSpellFragrance = 58921041;
        }

        public BattleCmdResponse OnBattleCmd(BattleCmdData state)
        {
            // Sort the attackers and defenders, make monster with higher attack go first.
            IEnumerable<DynamicCard> attackersSelectable = new List<DynamicCard>(state.AttackableCards);
            IEnumerable<DynamicCard> attackersSorted = attackersSelectable.Where(x => x.Attack >= 0).OrderByDescending(c => c.Attack);

            List<DynamicCard> defenders = new();
            defenders.AddRange(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result.ToList());
            defenders.AddRange(FieldHelpers.GetMonsterInFaceDown(BotWrapper, Controller.Player).Result.ToList());

            //List<DynamicCard> defenders = new(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result);
            IEnumerable<DynamicCard> defendersSorted = defenders.Where(d => defenders.Any(e => e.GetRealPower() >= d.GetRealPower())).OrderByDescending(f => f.GetRealPower());

            int pos = 0;

            foreach (DynamicCard attackerSelectable in attackersSelectable)
            {
                foreach (DynamicCard attacker in attackersSorted)
                {
                    if (attacker.GetHashCode().Equals(attackerSelectable.GetHashCode()))
                    {
                        if (defendersSorted.Any())
                        {
                            foreach (DynamicCard defender in defendersSorted)
                            {
                                if (!OnPreBattleBetween(attacker, defender, BotWrapper))
                                    continue;

                                if (attacker.GetRealPower() > defender.GetRealPower())
                                {
                                    return state.AttackableCards[pos].DeclareAttack();
                                }
                                //else
                                //{
                                //    pos++;
                                //}
                            }
                            pos++;
                        }
                        else
                        {
                            return state.AttackableCards.Where(a => attackersSelectable.Any(b => b.Attack >= a.Attack)).OrderByDescending(c => c.Attack).First().DeclareAttack();
                        }
                    }
                }
            }

            if (state.CanMainPhaseTwo)
                return BattleAction.ToMainPhaseTwo;

            return BattleAction.ToEndPhase;
        }

        /// <summary>
        /// Decide whether to declare attack between attacker and defender.
        /// Can be overrided to update the RealPower of attacker for cards like Honest.
        /// </summary>
        /// <param name="attacker">Card that attack.</param>
        /// <param name="defender">Card that defend.</param>
        /// <returns>false if the attack shouldn't be done.</returns>
        public bool OnPreBattleBetween(DynamicCard attacker, DynamicCard defender, DuelBotWrapper BotWrapper)
        {
            CardInfo Bot = BotWrapper.GetBotFieldInfo().Result.OwnSide;

            CardInfo Enemy = BotWrapper.GetBotFieldInfo().Result.EnemySide;

            if (!attacker.IsMonsterHasPreventActivationEffectInBattle())
            {
                if (defender.IsMonsterInvincible() && defender.IsDefense())
                    return false;

                if (defender.IsMonsterDangerous())
                {
                    bool canIgnoreIt = !attacker.IsDisabled() && (
                        attacker.IsCode(CardId.UltimateConductorTytanno) && defender.IsDefense() ||
                        attacker.IsCode(CardId.ElShaddollConstruct) && defender.IsSpecialSummoned ||
                        attacker.IsCode(CardId.AllyOfJusticeCatastor) && !defender.HasAttribute(CardAttribute.Dark));
                    if (!canIgnoreIt)
                        return false;
                }

                foreach (DynamicCard equip in defender.EquipCards)
                {
                    if (equip.IsCode(CardId.MoonMirrorShield) && !equip.IsDisabled())
                    {
                        return false;
                    }
                }

                if (!defender.IsDisabled())
                {
                    if (defender.IsCode(CardId.MekkKnightCrusadiaAstram) && defender.IsAttack() && attacker.IsSpecialSummoned)
                        return false;

                    if (defender.IsCode(CardId.CrystalWingSynchroDragon) && defender.IsAttack() && attacker.Level >= 5)
                        return false;

                    if (defender.IsCode(CardId.AllyOfJusticeCatastor) && !attacker.HasAttribute(CardAttribute.Dark))
                        return false;

                    if (defender.IsCode(CardId.NumberS39UtopiaTheLightning) && defender.IsAttack() && defender.HasXyzMaterial(2, CardId.Number39Utopia))
                        defender.Attack = 5000;

                    if (defender.IsCode(CardId.VampireFraeulein))
                        defender.Attack += (Bot.LifePoints > 3000) ? 3000 : (Enemy.LifePoints - 100);

                    if (defender.IsCode(CardId.InjectionFairyLily) && Enemy.LifePoints > 2000)
                        defender.Attack += 3000;
                }
            }

            if (!defender.IsMonsterHasPreventActivationEffectInBattle())
            {
                if (attacker.IsCode(CardId.NumberS39UtopiaTheLightning) && !attacker.IsDisabled() && attacker.HasXyzMaterial(2, CardId.Number39Utopia))
                    attacker.Attack = 5000;

                if (attacker.IsCode(CardId.EaterOfMillions) && !attacker.IsDisabled())
                    attacker.Attack = 9999;

                if (attacker.IsMonsterInvincible())
                    attacker.Attack = 9999;

                foreach (DynamicCard equip in attacker.EquipCards)
                {
                    if (equip.IsCode(CardId.MoonMirrorShield) && !equip.IsDisabled())
                    {
                        attacker.Attack = defender.Attack + 100;
                    }
                }
            }

            List<DynamicCard> EnemiesMonstersZone = new(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result);

            if (EnemiesMonstersZone.TryGetByID(CardId.MekkKnightCrusadiaAstram) && !defender.IsCode(CardId.MekkKnightCrusadiaAstram))
                return false;

            if (EnemiesMonstersZone.TryGetByID(CardId.DupeFrog) && !defender.IsCode(CardId.DupeFrog))
                return false;

            if (EnemiesMonstersZone.TryGetByID(CardId.MaraudingCaptain) && !defender.IsCode(CardId.MaraudingCaptain) && defender.Race.Equals(Enum.GetValues(CardRace.Warrior.GetType())))
                return false;

            if (defender.IsCode(CardId.UltimayaTzolkin) && !defender.IsDisabled() && EnemiesMonstersZone.Any(monster => !monster.Equals(defender) && monster.HasType(CardType.Synchro)))
                return false;

            if (EnemiesMonstersZone.Any(monster => !monster.Equals(defender) && monster.IsCode(CardId.HamonLordofStrikingThunder) && !monster.IsDisabled() && monster.IsDefense()))
                return false;

            if (defender.OwnTargets.Any(card => card.IsCode(CardId.PhantomKnightsFogBlade) && !card.IsDisabled()))
                return false;

            return true;
        }


        public IdleCmdResponse OnIdleCmd(IdleCmdData state)
        {
            IsBotCardDefault = false;

            //Spell
            if (state.ActivableCards.Where(card => (card.Type == (int)CardType.Spell || card.Type == (int)CardType.QuickPlay) && card.Cover == 0).Any())
            {
                var spellActionsResponse = SpellExecutor.SpellCardActivation(state, BotWrapper);
                if (spellActionsResponse.Item1)
                {
                    int cardID = state.ActivableCards[spellActionsResponse.Item2].ID;

                    if (cardID == (int)SpellCardID.AllureOfDarkness)
                        IsBotCardDefault = true;

                    return state.ActivableCards[spellActionsResponse.Item2].MainActivateFromIndex();
                }
            }

            if (state.ActivableCards.Where(card => (card.Type != (int)CardType.Spell && card.Type != (int)CardType.QuickPlay) && card.Cover == 0).Any())
            {
                int index = Array.FindIndex(state.ActivableCards, card => card.Type != (int)CardType.Spell && card.Type != (int)CardType.QuickPlay && !card.IsDisabled());
                return state.ActivableCards[index].MainActivateFromIndex();
            }

            //Trap
            if (state.SpellSetableCards.Where(card => card.Type == (int)CardType.Trap || card.Type == (int)CardType.TrapMonster || card.Type == (int)CardType.Continuous).Any())
                return state.SpellSetableCards.Where(card => card.Type == (int)CardType.Trap || card.Type == (int)CardType.TrapMonster || card.Type == (int)CardType.Continuous).First().SetSpellTrap();

            if (state.SpecialSummonableCards.Any())
                return state.SpecialSummonableCards.First().SpecialSummon();

            if (state.SummonableCards.Any())
                return state.SummonableCards.First().NormalSummon();


            //Skill
            if (state.ActivableCards.Where(a => a.Cover != 0).Any())
            {
                IsBotCardDefault = true;

                int index = Array.FindIndex(state.ActivableCards, card => card.Cover != 0 && !card.IsDisabled());

                DynamicCard card = state.ActivableCards[index];

                var SkillActionsResponse = SkillExector.SkillCardActivation(state, BotWrapper);

                if (SkillActionsResponse.Item1)
                    return state.ActivableCards[SkillActionsResponse.Item2].MainActivateFromIndex();
            }

            if (state.CanBattlePhase)
                return MainAction.ToBattlePhase;
            return MainAction.ToEndPhase;
        }

        public int OnSelectChain(SelectChainData state)
        {
            //Dust Tornado
            if (state.SelectableCards.TryGetByID((int)TrapCardID.DustTornado) &&
                FieldHelpers.GetSpellTrapInFaceDown(BotWrapper, Controller.Player).Result.Count > 0 &&
                !BotWrapper.CurrentStateHeader.CurrentPhase.Equals((byte)DuelPhase.BattleStart) &&
                !FieldHelpers.TryGetSpellTrapByID(BotWrapper, Controller.Bot, (int)TrapCardID.DustTornado).Result)
            {
                CardID = (int)TrapCardID.DustTornado;
                int index = Array.FindIndex(state.SelectableCards, card => card.ID == (int)TrapCardID.DustTornado && !card.IsDisabled());
                return state.SelectableCards[index].ActionIndex[(int)MainAction.Activate];
            }

            //Active IF the SelectableCard is Floodgate Trap Hole
            if (TrapActions.TrapCardActivation(state.SelectableCards, BotWrapper))
                return state.SelectableCards[0].ActionIndex[(int)MainAction.Activate];

            var bot = BotWrapper.GetBotFieldInfo().Result.OwnSide;

            //Active trap in Battle Phase, my oponent attacking
            if (BotWrapper.CurrentStateHeader.CurrentPhase.Equals((byte)DuelPhase.BattleStart) &&
                !state.SelectableCards.TryGetByID((int)TrapCardID.LostWind))
                if (bot.UnderAttack)
                    if (!state.Forced)
                        return state.SelectableCards[0].ActionIndex[(int)MainAction.Activate];

            //Spell Actions
            var spellActionsResponse = SpellExecutor.SpellCardActivation(state, BotWrapper);

            if (spellActionsResponse.Item1)
            {

                int cardID = state.SelectableCards[spellActionsResponse.Item2].ID;

                return state.SelectableCards[spellActionsResponse.Item2].ActionIndex[(int)MainAction.Activate];
            }

            return -1;
        }

        public int OnSelectChain(SelectChainData state, int cards)
        {
            //Dust Tornado
            if (cards.Equals(TrapCardID.DustTornado))
            {
                int index = Array.FindIndex(state.SelectableCards, card => card.ID == (int)cards && !card.IsDisabled());
                return state.SelectableCards[index].ActionIndex[(int)MainAction.Activate];
            }

            //Active IF the SelectableCard is Floodgate Trap Hole
            if (TrapActions.TrapCardActivation(state.SelectableCards, BotWrapper))
                return state.SelectableCards[0].ActionIndex[(int)MainAction.Activate];

            var bot = BotWrapper.GetBotFieldInfo().Result.OwnSide;

            //Active trap in Battle Phase, my oponent attacking
            if (BotWrapper.CurrentStateHeader.CurrentPhase.Equals((byte)DuelPhase.BattleStart))
                if (bot.UnderAttack)
                    if (!state.Forced)
                        return state.SelectableCards[0].ActionIndex[(int)MainAction.Activate];

            return -1;
        }

        public IList<byte>? OnSelectCard(SelectCardData state, int cardId)
        {
            Random rnd = new();
            byte[] selection = new byte[state.Minimum];
            
            if (state.SelectionHint.Equals((int)DHint.DESTROY))
            {
                int index = 0;

                if (cardId.Equals((int)TrapCardID.DustTornado))
                {
                    index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Player && (card.IsFacedown()) || card.IsEquipCard() || card.IsPendulumCard() && !card.IsDisabled());
                }

                for (int i = 0; i < selection.Length; i++)
                {
                    selection[i] = (byte)index;
                }

            }
            else
            {
                for (int i = 0; i < selection.Length; i++)
                    selection[i] = (byte)i;
            }

            return selection;
        }

        public IList<byte>? OnSelectCard(SelectCardData state, bool IsBotCard)
        {
            Random rnd = new();
            byte[] selection = new byte[state.Minimum];

            if (state.SelectionHint.Equals((int)DHint.REMOVE))
            {
                //If Minimum Remove >= 3
                if(state.Minimum >= 3)
                {
                    for (int i = 0; i < selection.Length; i++)
                        selection[i] = (byte)i;
                }
                else
                {
                    OnSelectCardRandom(state, IsBotCard);

                }
            }
            else if (state.SelectionHint.Equals((int)DHint.DESTROY))
            {
                OnSelectCardRandom(state, IsBotCard);
            }
            else if (state.SelectionHint.Equals((int)DHint.FACEUP))
            {
                DynamicCard[] faceUpSelectable = state.SelectableCards;
                var levelMax = faceUpSelectable.Max(levelMax => levelMax.Level);
                int index;

                if (IsBotCard)
                    index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Bot && (card.Level == levelMax) && !card.IsDisabled());
                else
                    index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Player && (card.Level == levelMax) && !card.IsDisabled());


                for (int i = 0; i < selection.Length; i++)
                {
                    selection[i] = (byte)index;
                }
            }
            else
            {
                for (int i = 0; i < selection.Length; i++)
                    selection[i] = (byte)i;
            }

            return selection;
        }

        public IList<byte>? OnSelectCard(SelectCardData state)
        {
            Random rnd = new();
            byte[] selection = new byte[state.Minimum];

            if (state.SelectionHint.Equals((int)DHint.REMOVE))
            {
                selection = OnSelectCardRandom(state, IsBotCardDefault);
            }
            else if (state.SelectionHint.Equals((int)DHint.DESTROY))
            {
                selection = OnSelectCardRandom(state, IsBotCardDefault);
            }
            else if (state.SelectionHint.Equals((int)DHint.FACEUP))
            {
                DynamicCard[] faceUpSelectable = state.SelectableCards;

                if (CardID.Equals((int)TrapCardID.DustTornado))
                {
                    int index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Player && (card.IsFacedown() || card.IsEquipCard() || card.IsPendulumCard()) && !card.IsDisabled());
                    
                    for (int i = 0; i < selection.Length; i++)
                    {
                        selection[i] = (byte)index;
                    }
                }

                else if (faceUpSelectable.Length >= 1)
                {
                    //var levelMax = faceUpSelectable.Max(levelMax => levelMax.Level);
                    int index;

                    if (IsBotCardDefault)
                        index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Bot && !card.IsDisabled());
                    else
                        index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Player && !card.IsDisabled());

                    for (int i = 0; i < selection.Length; i++)
                    {
                        selection[i] = (byte)index;
                    }
                }
                else
                {
                    for (int i = 0; i < selection.Length; i++)
                        selection[i] = (byte)i;
                }
            }
            else if (state.SelectionHint.Equals((int)DHint.ATTACKTARGET))
            {
                DynamicCard[] atacktargetSelectable = state.SelectableCards;
                var realPowerMin = atacktargetSelectable.Min(realPower => realPower.GetRealPower());
                int index;

                if (IsBotCardDefault)
                    index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Bot && (card.GetRealPower() == realPowerMin) && !card.IsDisabled());
                else
                    index = Array.FindIndex(state.SelectableCards, card => card.Owner == (int)Controller.Player && (card.GetRealPower() == realPowerMin) && !card.IsDisabled());

                for (int i = 0; i < selection.Length; i++)
                {
                    selection[i] = (byte)index;
                }
            }
            else
            {
                for (int i = 0; i < selection.Length; i++)
                    selection[i] = (byte)i;
            }

            return selection;
        }

        public bool OnSelectEffectYn(SelectEffectYnData state)
        {
            return false;
        }

        public bool OnSelectYesNo(SelectYesNoData state)
        {
            return true;
        }

        public int OnSelectOption(SelectOptionData state)
        {
            return 0;
        }

       public IList<byte> OnSelectUnselectCard(SelectUnselectCardData state)
        {
            List<byte> selection = new();
            for (int i = 0; i < state.Minimum; i++)
                selection.Add((byte)i);
            return selection;
        }

        private byte[] OnSelectCardRandom(SelectCardData state, bool IsBotCard)
        {
            Random rnd = new();
            byte[] selection = new byte[state.Minimum];

            int[] selectableTargets;

            DynamicCard[] selectable = state.SelectableCards;

            if (IsBotCard)
                selectableTargets = new int[selectable.Where(card => card.Owner == (int)Controller.Bot && !card.IsDisabled()).Count()];
            else
                selectableTargets = new int[selectable.Where(card => card.Owner == (int)Controller.Player && !card.IsDisabled()).Count()];

            int pos = 0;

            if (!IsBotCard)
            {
                for (int i = 0; i < selectable.Length; i++)
                {
                    if (selectable[i].Owner == (int)Controller.Player)
                    {
                        selectableTargets[pos] = i;
                        pos++;
                    }
                }
            }
            else if (IsBotCard)
            {
                for (int i = 0; i < selectable.Length; i++)
                {
                    if (selectable[i].Owner == (int)Controller.Player)
                    {
                        selectableTargets[pos] = i;
                        pos++;
                    }
                }
            }

            int index_random = rnd.Next(selectableTargets.Length);
            int index_target = selectableTargets[index_random];

            for (int i = 0; i < selection.Length; i++)
            {
                selection[i] = (byte)index_target;
            }

            return selection;
        }
    }
}
