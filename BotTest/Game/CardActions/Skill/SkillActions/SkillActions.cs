﻿using BotTest.Game.CardActions.Spell.Enums;
using BotTest.Game.Helpers.Enums;
using BotTest.Game.Helpers;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
using System.Threading;
using BotTest.Game.CardActions.Skill.Enums;
using System.Formats.Asn1;
#pragma warning disable CS8604
namespace BotTest.Game.CardActions.Skill.SkillActions
{
    public class SkillActions
    {
        public DuelBotWrapper? BotWrapper { get; set; }

        public void GetDuelBotWrapper(DuelBotWrapper bot)
        {
            this.BotWrapper = bot;
        }

        public static Tuple<bool, int> SkillCardActivation(IdleCmdData state)
        {
            List<DynamicCard> SkillCard = new(state.ActivableCards.Where(a => a.Cover != 0));

            if (SkillCard.Any())
            {
                int index = Array.FindIndex(state.ActivableCards, card => card.Cover != 0 && !card.IsDisabled());
                return new(true, index);
            }
            return new(false, -1);

        }

        public Tuple<bool, int> SkillCardActivation(IdleCmdData state, DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> SkillCard = new(state.ActivableCards.Where(a => a.Cover != 0));

            int index = Array.FindIndex(state.ActivableCards, card => card.Cover != 0 && !card.IsDisabled());

            int cardID = state.ActivableCards[index].ID;

            if (index >= 0)
            {

                //I'm Just Gonna Attack!
                if(cardID == (int)SkillCardID.ImJustGonnaAttack)
                {
                    if (ImJustGonnaAttack(BotWrapper))
                        return new(true, index);
                }
                else if(cardID != (int)SkillCardID.ImJustGonnaAttack)
                {
                    return new(true, index);
                }
            }
            return new(false, -1);
        }

        private static bool ImJustGonnaAttack(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> monstersBot = new(FieldHelpers.GetMonster(BotWrapper, Controller.Bot).Result.Where(monster => monster.IsFaceup() && !monster.HasType(CardType.Continuous) && !monster.HasType(CardType.Link) && monster.HasType(CardType.Xyz) || monster.Level > 4));

            if (monstersBot.Count == 0)
                return false;

            if (monstersBot.Count > 0)
                return true;

            return false;
        }
    }
}
