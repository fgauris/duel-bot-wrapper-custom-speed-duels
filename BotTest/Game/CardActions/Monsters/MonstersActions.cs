﻿using BotTest.Game.CardActions.Monsters.Effects.Enums;
using BotTest.Game.CardActions.Spell.Enums;
using BotTest.Game.Helpers.Enums;
using BotTest.Game.Helpers;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
#pragma warning disable CS8604


namespace BotTest.Game.CardActions.Monsters
{
    public class MonstersActions
    {
        public DuelBotWrapper? BotWrapper { get; set; }

        public void GetDuelBotWrapper(DuelBotWrapper bot)
        {
            this.BotWrapper = bot;
        }

        public Tuple<bool, int> MonsterEffectActivation(IdleCmdData state, DuelBotWrapper BotWrapper)
        {
            var arrays = state.ActivableCards;

            for (int i = 0; i < arrays.Length; i++)
            {
                int index = Array.FindIndex(state.ActivableCards, card => (card.Type == (int)CardType.Monster || card.Type == (int)CardType.Effect) && !card.IsDisabled());
                DynamicCard cardId = state.ActivableCards[index];

                if (index >= 0)
                {
                    switch (cardId.ID)
                    {
                        case (int)MonstersEffectsID.ExiledForce:
                            if (ExiledForce(BotWrapper))
                                return new(true, index);
                            break;

                        default:
                            return new(true, index);
                    }
                }
            }

            return new(false, -1);
        }

        private static bool ExiledForce(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> monstersPlayer = new(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result.Where(monster => monster.IsFaceup() && monster.Level > 2));

            if (monstersPlayer.Count == 0)
                return false;

            if (monstersPlayer.Count > 0)
                return true;

            return false;
        }

    }
}
