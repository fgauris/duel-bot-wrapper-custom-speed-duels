﻿namespace BotTest.Game.CardActions.Monsters.Effects.Enums
{
    public enum MonstersEffectsID
    {
        BeakerTheMagicalWarrior = 71413901,
        Jinzo = 77585513,
        ExiledForce = 74131780,
        DDWarriorLady = 7572887,
        LavaGolem = 102380
    }
}
