﻿using BotTest.Game.CardActions.Monsters.Effects.Enums;
using BotTest.Game.Helpers;
using BotTest.Game.Helpers.Enums;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.Struct;
#pragma warning disable CS8604
namespace BotTest.Game.CardActions.Monsters.Effects.EffectsActions
{
    public class MonstersEffectsActions
    {
        public DuelBotWrapper? BotWrapper { get; set; }

        public void GetDuelBotWrapper(DuelBotWrapper bot)
        {
            this.BotWrapper = bot;
        }

        public static bool MonsterEffectsActions(DynamicCard[] ActivableCards, DuelBotWrapper BotWrapper)
        {
            IEnumerable<DynamicCard> monstersEffectsActivable = ActivableCards.Where(x => x.Type.Equals((int)CardType.Effect));

            foreach (DynamicCard monsterEffect in monstersEffectsActivable)
            {
                switch (monsterEffect.ID)
                {
                    case (int)MonstersEffectsID.BeakerTheMagicalWarrior:
                        List<DynamicCard> spellTrapsCount = new(FieldHelpers.GetSpellTrap(BotWrapper, Controller.Player).Result);
                        if (spellTrapsCount.Count > 0)
                            return true;
                        break;

                    default: return false;
                }
            }
            return false;
        }
    }
}
