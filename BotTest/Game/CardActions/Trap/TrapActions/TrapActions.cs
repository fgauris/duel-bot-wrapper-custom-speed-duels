﻿using BotTest.Game.CardActions.Spell.Enums;
using BotTest.Game.CardActions.Trap.Enums;
using BotTest.Game.Helpers;
using BotTest.Game.Helpers.Enums;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
#pragma warning disable CS8604
namespace BotTest.Game.CardActions.Trap.TrapActions
{
    public class TrapActions
    {
        public static bool TrapCardActivation(DynamicCard[] ActivableCards, DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> TrapsFaceUp = new(FieldHelpers.GetSpellTrap(BotWrapper, Controller.Bot).Result.Where(a => a.IsFaceup() && a.Type.Equals((int)CardType.Trap)));

            IEnumerable<DynamicCard> trapActivable = ActivableCards.Where(x => x.Type.Equals((int)CardType.Trap));
            IEnumerable<DynamicCard> trapsMatched = TrapsFaceUp.Where(d => TrapsFaceUp.Any(trapActivable => trapActivable.ID == d.ID));

            //to avoid to use same trap in selectChain
            trapsMatched = TrapsFaceUp.Where(d => TrapsFaceUp.Any(trapActivable => trapActivable.ID == d.ID));

            if (!trapsMatched.Any())
            {
                foreach (var cardActivable in trapActivable)
                {
                    return cardActivable.ID switch
                    {
                        (int)TrapCardID.FloodgateTrapHole => true,
                        (int)TrapCardID.WallOfDisruption => true,
                        _ => false,
                    };
                }
            }
            return false;
        }
    }
}
