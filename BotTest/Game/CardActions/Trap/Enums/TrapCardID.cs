﻿namespace BotTest.Game.CardActions.Trap.Enums
{
    public enum TrapCardID
    {
        FloodgateTrapHole = 69599136,
        WallOfDisruption = 58169732,
        Waboku = 12607053,
        LostWind = 74003290,
        DustTornado = 60082869,
    }
}
