﻿namespace BotTest.Game.CardActions.Spell.Enums
{
    public enum SpellCardID
    {
        AllureOfDarkness = 1475311,
        HammerShot = 26412047,
        CosmicCyclone = 08267140,
        BookOfMoon = 14087893,
        EnchantingFittingRoom = 30531525,
        OfferingsToTheDoomed = 19230408,
        NoblemanOfCrossout = 71044499
    }
}
