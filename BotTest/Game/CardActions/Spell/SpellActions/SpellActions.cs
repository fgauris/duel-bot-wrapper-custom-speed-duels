﻿using BotTest.Game.CardActions.Skill.Enums;
using BotTest.Game.CardActions.Spell.Enums;
using BotTest.Game.Helpers;
using BotTest.Game.Helpers.Enums;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
#pragma warning disable CS8604
namespace BotTest.Game.CardActions.SpellActions.Enums
{
    public class SpellActions
    {
        public DuelBotWrapper? BotWrapper { get; set; }

        public void GetDuelBotWrapper(DuelBotWrapper bot)
        {
            this.BotWrapper = bot;
        }

        public Tuple<bool, int> SpellCardActivation(IdleCmdData state, int[] arrays)
        {
            for (int i = 0; i < arrays.Length; i++)
            {
                int index = Array.FindIndex(state.ActivableCards, card => card.ID == arrays[i] && !card.IsDisabled());
                
                if (index >= 0)
                {
                    switch (arrays[i])
                    {
                        case (int)SpellCardID.AllureOfDarkness:
                            if (AllureOfDarkness(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.HammerShot:
                            if (HammerShot(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.CosmicCyclone:
                            if (CosmicCyclone(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.BookOfMoon:
                            if (BookOfMoon(BotWrapper))
                                return new(true, index);
                            break;

                        default:
                            return new(true, index);
                    }
                }
            }

            return new(false, -1);
        }

        public Tuple<bool, int> SpellCardActivation(IdleCmdData state, DuelBotWrapper BotWrapper)
        {
            var arrays = state.ActivableCards;

            for (int i = 0; i < arrays.Length; i++)
            {
                int index = Array.FindIndex(state.ActivableCards, card => (card.Type == (int)CardType.Spell || card.Type == (int)CardType.QuickPlay) && !card.IsDisabled());
                DynamicCard cardId = state.ActivableCards[index];

                if (index >= 0)
                {
                    switch (cardId.ID)
                    {
                        case (int)SpellCardID.AllureOfDarkness:
                            if (AllureOfDarkness(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.HammerShot:
                            if (HammerShot(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.CosmicCyclone:
                            if (CosmicCyclone(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.BookOfMoon:
                            if (BookOfMoon(BotWrapper))
                                return new(true, index);
                            break;
                        case (int)SpellCardID.OfferingsToTheDoomed:
                            if (OfferingsToTheDoomed(BotWrapper))
                                return new(true, index);
                            break;

                        default:
                            return new(true, index);
                    }
                }
            }

            return new(false, -1);
        }

        public Tuple<bool, int> SpellCardActivation(SelectChainData state, DuelBotWrapper BotWrapper)
        {
            var arrays = state.SelectableCards;

            for (int i = 0; i < arrays.Length; i++)
            {
                int index = Array.FindIndex(state.SelectableCards, card => (card.Type == (int)CardType.Spell || card.Type == (int)CardType.QuickPlay) && !card.IsDisabled());
                DynamicCard cardId = state.SelectableCards[index];

                if (index >= 0)
                {
                    switch (cardId.ID)
                    {
                        case (int)SpellCardID.AllureOfDarkness:
                            if (AllureOfDarkness(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.HammerShot:
                            if (HammerShot(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.CosmicCyclone:
                            if (CosmicCyclone(BotWrapper))
                                return new(true, index);
                            break;

                        case (int)SpellCardID.BookOfMoon:
                            if (BookOfMoon(BotWrapper))
                                return new(true, index);
                            break;
                        case (int)SpellCardID.OfferingsToTheDoomed:
                            if (OfferingsToTheDoomed(BotWrapper))
                                return new(true, index);
                            break;

                        default:
                            return new(true, index);
                    }
                }
            }

            return new(false, -1);
        }

        private static bool AllureOfDarkness(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> hands = new(FieldHelpers.GetHand(BotWrapper, Controller.Player).Result);

            if (hands.Count > 0)
                if (hands.Where(card => card.Attribute.Equals((byte)CardAttribute.Dark)).Any())
                    return true;

            return false;
        }

        private static bool HammerShot(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> monstersBot = new(FieldHelpers.GetMonster(BotWrapper, Controller.Bot).Result.Where(a => a.IsAttack()));
            List<DynamicCard> monstersPlayer = new(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result.Where(b => b.IsAttack()));

            if (monstersPlayer.Count == 0)
                return false;

            if (monstersBot.Count == 0 && monstersPlayer.Count > 0)
                return true;

            if (monstersPlayer.Max(atkMax => atkMax.Attack) > monstersBot.Max(atkMax => atkMax.Attack))
                return true;

            return false;
        }

        private static bool CosmicCyclone(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> spellsTraps = new(FieldHelpers.GetSpellTrap(BotWrapper, Controller.Player).Result);

            if (spellsTraps.Count > 0)
                return true;

            return false;

        }

        private static bool BookOfMoon(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> monstersPlayer = new(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result.Where(monster => monster.IsFaceup() && monster.HasType(CardType.Effect) && !monster.HasType(CardType.Link) && monster.HasType(CardType.Xyz) || monster.Level > 4));

            if (monstersPlayer.Count == 0)
                return false;

            if (monstersPlayer.Count > 0)
                return true;

            return false;
        }

        private static bool OfferingsToTheDoomed(DuelBotWrapper BotWrapper)
        {
            List<DynamicCard> monstersPlayer = new(FieldHelpers.GetMonster(BotWrapper, Controller.Player).Result.Where(monster => monster.IsFaceup() && !monster.HasType(CardType.Link) && monster.HasType(CardType.Xyz) || monster.Level > 2));

            if (monstersPlayer.Count == 0)
                return false;

            if (monstersPlayer.Count > 0)
                return true;

            return false;
        }
    }
}
