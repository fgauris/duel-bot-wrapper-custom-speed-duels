﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BotTest.Game.Helpers.Enums
{
    public enum Controller
    {
        Bot = 0,
        Player = 1
    }
}
