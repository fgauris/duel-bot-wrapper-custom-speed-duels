﻿using BotTest.Game.Helpers.Enums;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.Struct;
#pragma warning disable CS8600
#pragma warning disable CS8604

namespace BotTest.Game.Helpers
{
    public class FieldHelpers
    {
        public DuelBotWrapper? BotWrapper { get; set; }

        public void GetDuelBotWrapper(DuelBotWrapper bot)
        {
            this.BotWrapper = bot;
        }

        public static async Task<int> GetMonsterCount(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            switch (Controller)
            {
                case Controller.Bot:
                    return BotFieldInfo.OwnSide.MonsterZones.Count(card => card.Cards.Any(b => b != 0));

                case Controller.Player:
                    return BotFieldInfo.EnemySide.MonsterZones.Count(card => card.Cards.Any(b => b != 0));

                default: return 0;
            }
        }

        public static async Task<int> GetMainDeckCount(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            return Controller switch
            {
                Controller.Bot => BotFieldInfo.OwnSide.Main.Length,
                Controller.Player => BotFieldInfo.EnemySide.Main.Length,
                _ => 0,
            };
        }

        public static async Task<List<DynamicCard>?> GetMonster(DuelBotWrapper BotWrapper, Controller Controller)
        {
            try
            {
                FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

                List<DynamicCard> monsters = new();

                int PlayerSequences = 0;

                switch (Controller)
                {
                    case Controller.Bot:
                        foreach (var monster in BotFieldInfo.OwnSide.MonsterZones)
                        {
                            var monstersID = monster.Cards.FirstOrDefault();

                            if (monstersID != 0)
                            {
                                DynamicCard BotCardInfo = await BotWrapper.GetBotFieldCardInfo(0, (byte)CardLocation.MonsterZone, PlayerSequences);

                                monsters.Add(BotCardInfo);

                                PlayerSequences++;
                            }
                            else
                            {
                                PlayerSequences++;
                            }
                        }
                        return monsters;

                    case Controller.Player:
                        foreach (var monster in BotFieldInfo.EnemySide.MonsterZones)
                        {
                            var monstersID = monster.Cards.FirstOrDefault();

                            if (monstersID != 0)
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo(1, (byte)CardLocation.MonsterZone, PlayerSequences);

                                monsters.Add(PlayerCardInfo);

                                PlayerSequences++;
                            }
                            else
                            {
                                PlayerSequences++;
                            }
                        }

                        return monsters;

                    default: return null;
                }
            } catch (Exception ex) { }

            return null;
        }

        public static async Task<List<DynamicCard>?> GetMonsterInFaceDown(DuelBotWrapper BotWrapper, Controller Controller)
        {
            try
            {
                List<DynamicCard> monstersInFaceDown = new();

                int PlayerSequences = 0;

                switch (Controller)
                {
                    case Controller.Bot:
                        for (int i = 0; i < 7; i++)
                        {
                            try
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo((int)Controller.Bot, (byte)CardLocation.MonsterZone, PlayerSequences);

                                if (PlayerCardInfo.Position.Equals((byte)CardPosition.FaceDownDefence) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceDownAttack) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceDown))
                                {
                                    monstersInFaceDown.Add(PlayerCardInfo);
                                }
                            }
                            catch
                            {

                            }
                        }
                        return monstersInFaceDown;

                    case Controller.Player:
                        for (int i = 0; i < 7; i++)
                        {
                            try
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo((int)Controller.Player, (byte)CardLocation.MonsterZone, i);

                                if (PlayerCardInfo.Position.Equals((byte)CardPosition.FaceDownDefence))
                                {
                                    monstersInFaceDown.Add(PlayerCardInfo);
                                }
                            }
                            catch
                            {

                            }
                        }
                        return monstersInFaceDown;

                    default: return null;
                }
            }
            catch (Exception ex) { }

            return null;
        }

        public static async Task<List<DynamicCard>?> GetMonsterInFaceUp(DuelBotWrapper BotWrapper, Controller Controller)
        {
            try
            {
                List<DynamicCard> monstersInFaceUp = new();

                int PlayerSequences = 0;

                switch (Controller)
                {
                    case Controller.Bot:
                        for (int i = 0; i < 7; i++)
                        {
                            try
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo((int)Controller.Bot, (byte)CardLocation.MonsterZone, PlayerSequences);

                                if (PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUp) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUpDefence) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUpAttack))
                                {
                                    monstersInFaceUp.Add(PlayerCardInfo);
                                }
                            }
                            catch
                            {

                            }
                        }
                        return monstersInFaceUp;

                    case Controller.Player:
                        for (int i = 0; i < 7; i++)
                        {
                            try
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo((int)Controller.Player, (byte)CardLocation.MonsterZone, i);

                                if (PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUp) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUpAttack))
                                {
                                    monstersInFaceUp.Add(PlayerCardInfo);
                                }
                            }
                            catch
                            {

                            }
                        }
                        return monstersInFaceUp;

                    default: return null;
                }
            }
            catch (Exception ex) { }

            return null;
        }

        public static async Task<IEnumerable<DynamicCard>?> GetMonsterInFaceUpSpecialSummoned(DuelBotWrapper BotWrapper, Controller Controller)
        {
            try
            {
                List<DynamicCard> monstersInFaceUpSpecialSummoned = new();

                int PlayerSequences = 0;

                switch (Controller)
                {
                    case Controller.Bot:
                        for (int i = 0; i < 7; i++)
                        {
                            try
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo((int)Controller.Bot, (byte)CardLocation.MonsterZone, PlayerSequences);

                                if (PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUp) || (PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUpDefence) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUpAttack)) && PlayerCardInfo.IsSpecialSummoned)
                                {
                                    monstersInFaceUpSpecialSummoned.Add(PlayerCardInfo);
                                }
                            }
                            catch
                            {

                            }
                        }
                        return monstersInFaceUpSpecialSummoned;

                    case Controller.Player:
                        for (int i = 0; i < 7; i++)
                        {
                            try
                            {
                                DynamicCard PlayerCardInfo = await BotWrapper.GetBotFieldCardInfo((int)Controller.Player, (byte)CardLocation.MonsterZone, i);

                                if ((PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUp) || PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUpAttack)) && PlayerCardInfo.IsSpecialSummoned)
                                {
                                    monstersInFaceUpSpecialSummoned.Add(PlayerCardInfo);
                                }
                            }
                            catch
                            {

                            }
                        }
                        return monstersInFaceUpSpecialSummoned;

                    default: return null;
                }
            }
            catch (Exception ex) { }

            return null;
        }

        public static async Task<int> GetSpellTrapCount(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            FieldInfo PlayerFieldInfo = await DuelBotWrapper.GetPlayerFieldInfo();

            switch (Controller)
            {
                case Controller.Bot:
                    return BotFieldInfo.OwnSide.SpellZones.Count(card => card.Cards.Any(b => b != 0));

                case Controller.Player:
                    return PlayerFieldInfo.OwnSide.SpellZones.Count(card => card.Cards.Any(b => b != 0));

                default: return 0;
            }
        }

        public static async Task<List<DynamicCard>?> GetSpellTrap(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            FieldInfo PlayerFieldInfo = await DuelBotWrapper.GetPlayerFieldInfo();

            List<DynamicCard> monsters = new();

            int PlayerSequences = 0;

            switch (Controller)
            {
                case Controller.Bot:
                    foreach (var monster in BotFieldInfo.OwnSide.SpellZones)
                    {
                        var monstersID = monster.Cards.FirstOrDefault();

                        if (monstersID != 0)
                        {
                            DynamicCard BotCardInfo = await BotWrapper.GetBotFieldCardInfo(0, (byte)CardLocation.SpellZone, PlayerSequences);

                            if (BotCardInfo.Cover == 0) //It's not Skill Card
                                monsters.Add(BotCardInfo);

                            PlayerSequences++;
                        }
                        else
                        {
                            PlayerSequences++;
                        }
                    }
                    return monsters;

                case Controller.Player:
                    foreach (var monster in PlayerFieldInfo.OwnSide.SpellZones)
                    {
                        var monstersID = monster.Cards.FirstOrDefault();

                        if (monstersID != 0)
                        {
                            DynamicCard PlayerCardInfo = await DuelBotWrapper.GetPlayerFieldCardInfo(0, (byte)CardLocation.SpellZone, PlayerSequences);

                            if (PlayerCardInfo.Cover == 0) //It's not Skill Card
                                monsters.Add(PlayerCardInfo);

                            PlayerSequences++;
                        }
                        else
                        {
                            PlayerSequences++;
                        }
                    }

                    return monsters;

                default: return null;
            }
        }

        public static async Task<List<DynamicCard>?> GetSpellTrapInFaceDown(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            FieldInfo PlayerFieldInfo = await DuelBotWrapper.GetPlayerFieldInfo();

            List<DynamicCard> monsters = new();

            int PlayerSequences = 0;

            switch (Controller)
            {
                case Controller.Bot:
                    foreach (var monster in BotFieldInfo.OwnSide.SpellZones)
                    {
                        var monstersID = monster.Cards.FirstOrDefault();

                        if (monstersID != 0)
                        {
                            DynamicCard BotCardInfo = await BotWrapper.GetBotFieldCardInfo(0, (byte)CardLocation.SpellZone, PlayerSequences);

                            if (BotCardInfo.Cover == 0 && BotCardInfo.Position.Equals((byte)CardPosition.FaceDown)) //It's not Skill Card
                                monsters.Add(BotCardInfo);

                            PlayerSequences++;
                        }
                        else
                        {
                            PlayerSequences++;
                        }
                    }
                    return monsters;

                case Controller.Player:
                    foreach (var monster in PlayerFieldInfo.OwnSide.SpellZones)
                    {
                        var monstersID = monster.Cards.FirstOrDefault();

                        if (monstersID != 0)
                        {
                            DynamicCard PlayerCardInfo = await DuelBotWrapper.GetPlayerFieldCardInfo(0, (byte)CardLocation.SpellZone, PlayerSequences);

                            if (PlayerCardInfo.Cover == 0 && PlayerCardInfo.Position.Equals((byte)CardPosition.FaceDown)) //It's not Skill Card
                                monsters.Add(PlayerCardInfo);

                            PlayerSequences++;
                        }
                        else
                        {
                            PlayerSequences++;
                        }
                    }

                    return monsters;

                default: return null;
            }
        }

        public static async Task<List<DynamicCard>?> GetSpellTrapInFaceUp(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            FieldInfo PlayerFieldInfo = await DuelBotWrapper.GetPlayerFieldInfo();

            List<DynamicCard> monsters = new();

            int PlayerSequences = 0;

            switch (Controller)
            {
                case Controller.Bot:
                    foreach (var monster in BotFieldInfo.OwnSide.SpellZones)
                    {
                        var monstersID = monster.Cards.FirstOrDefault();

                        if (monstersID != 0)
                        {
                            DynamicCard BotCardInfo = await BotWrapper.GetBotFieldCardInfo(0, (byte)CardLocation.SpellZone, PlayerSequences);

                            if (BotCardInfo.Cover == 0 && BotCardInfo.Position.Equals((byte)CardPosition.FaceUp)) //It's not Skill Card
                                monsters.Add(BotCardInfo);

                            PlayerSequences++;
                        }
                        else
                        {
                            PlayerSequences++;
                        }
                    }
                    return monsters;

                case Controller.Player:
                    foreach (var monster in PlayerFieldInfo.OwnSide.SpellZones)
                    {
                        var monstersID = monster.Cards.FirstOrDefault();

                        if (monstersID != 0)
                        {
                            DynamicCard PlayerCardInfo = await DuelBotWrapper.GetPlayerFieldCardInfo(0, (byte)CardLocation.SpellZone, PlayerSequences);

                            if (PlayerCardInfo.Cover == 0 && PlayerCardInfo.Position.Equals((byte)CardPosition.FaceUp)) //It's not Skill Card
                                monsters.Add(PlayerCardInfo);

                            PlayerSequences++;
                        }
                        else
                        {
                            PlayerSequences++;
                        }
                    }

                    return monsters;

                default: return null;
            }
        }
        public static async Task<List<DynamicCard>?> GetHand(DuelBotWrapper BotWrapper, Controller Controller)
        {
            FieldInfo BotFieldInfo = await BotWrapper.GetBotFieldInfo();

            FieldInfo PlayerFieldInfo = await DuelBotWrapper.GetPlayerFieldInfo();

            List<DynamicCard> cards = new();

            switch (Controller)
            {
                case Controller.Bot:
                    var handBot = BotFieldInfo.OwnSide.Hand.Length;

                    for (int sequence = 0; sequence < handBot; sequence++)
                    {
                        DynamicCard BotCardInfo = await BotWrapper.GetBotFieldCardInfo(0, (byte)CardLocation.Hand, sequence);

                        cards.Add(BotCardInfo);
                    }
                    return cards;

                case Controller.Player:
                    var handPlayer = PlayerFieldInfo.OwnSide.Hand.Length;

                    for (int sequence = 0; sequence < handPlayer; sequence++)
                    {
                        DynamicCard PlayerCardInfo = await DuelBotWrapper.GetPlayerFieldCardInfo(0, (byte)CardLocation.Hand, sequence);

                        cards.Add(PlayerCardInfo);
                    }
                    return cards;

                default: return null;
            }
        }

        public static async Task<bool> TryGetMonstersByID(DuelBotWrapper BotWrapper, Controller Controller, int id)
        {
            IEnumerable<DynamicCard> cards = await GetMonster(BotWrapper, Controller);
            if (id <= 0) return false;
            cards = cards.Where(c => c.ID == id || c.Alias == id);
            if (!cards.Any()) return false;
            return true;
        }

        public static async Task<bool> TryGetSpellTrapByID(DuelBotWrapper BotWrapper, Controller Controller, int id)
        {
            IEnumerable<DynamicCard> cards = await GetSpellTrapInFaceUp(BotWrapper, Controller);
            if (id <= 0) return false;
            cards = cards.Where(c => c.ID == id || c.Alias == id);
            if (!cards.Any()) return false;
            return true;
        }
    }
}
