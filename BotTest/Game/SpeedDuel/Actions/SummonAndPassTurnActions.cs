﻿using BotTest.Decks;
using BotTest.Decks.SpeedDuel.Enums;
using EnumsNET;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
#pragma warning disable 1998

namespace BotTest.Game.SpeedDuel.Actions
{
    public class SummonAndPassTurnActions : IBotActions
    {
        public string? DeckName { get; set; } = DeckOptions.SpeedDuel_Test_Summon_PassTurn.AsString(EnumFormat.Description);

        public string? DeckHash { get; set; } = DeckManager.DeckHashes(DeckOptions.SpeedDuel_Test_Summon_PassTurn);

        public DuelBotWrapper? BotWrapper { get; set; }

        public void RegisterActions(DuelBotWrapper bot)
        {
            bot.OnBattleCmdAction = OnBattleCmd;
            bot.OnIdleCmdAction = OnIdleCmd;
            bot.OnSelectChainAction = OnSelectChain;
            bot.OnSelectCardAction = OnSelectCard;
            bot.OnSelectTributeAction += OnSelectTribute;
            bot.OnSelectSumAction += OnSelectSum;
            bot.OnAnnounceAttributeAction += OnAnnounceAttribute;
            bot.OnAnnounceRaceAction += OnAnnounceRace;
            bot.OnAnnounceNumberAction += OnAnnounceNumber;
            bot.OnAnnounceCardAction += OnAnnounceCard;
            bot.OnAnnounceCardFilterAction += OnAnnounceCardFilter;
            bot.OnSelectUnselectCardAction += OnSelectUnselectCard;
            bot.OnSelectCounterAction += OnSelectCounter;
            bot.OnSelectDisfieldAction += OnSelectDisfield;
            bot.OnSelectPlaceAction += OnSelectPlace;
            bot.OnSelectEffectYnAction += OnSelectEffectYn;
            bot.OnSelectYesNoAction += OnSelectYesNo;
            bot.OnSelectOptionAction += OnSelectOption;
            bot.OnSelectRockPaperScissors += OnSelectRockPaperScissors;
            bot.OnSelectPositionAction += OnSelectPosition;
            bot.OnSortCardAction += OnSortCard;
        }

        async Task<BattleCmdResponse> OnBattleCmd(BattleCmdData state)
        {
            if (state.CanMainPhaseTwo)
                return BattleAction.ToMainPhaseTwo;
            return BattleAction.ToEndPhase;
        }

        async Task<IdleCmdResponse> OnIdleCmd(IdleCmdData state)
        {
            if (state.SpellSetableCards.Any())
                return state.SpellSetableCards.First().SetSpellTrap();
            if (state.SummonableCards.Any())
                return state.SummonableCards.First().NormalSummon();
            if (state.CanBattlePhase)
                return MainAction.ToBattlePhase;
            return MainAction.ToEndPhase;
        }
       
        async Task<int> OnSelectChain(SelectChainData state)
        {
            if (!state.Forced)
                return -1;
            return 0;
        }
       
        async Task<IList<byte>?> OnSelectCard(SelectCardData state)
        {
            byte[] selection = new byte[state.Minimum];
            for (int i = 0; i < selection.Length; i++)
                selection[i] = (byte)i;
            return selection;
        }
      
        async Task<IList<byte>?> OnSelectTribute(SelectTributeData state)
        {
            List<byte> resp = new();
            for (byte i = 0; i < state.Minimum; i++)
                resp.Add(i);
            return resp;
        }
       
        async Task<IList<byte>?> OnSelectSum(SelectSumData state)
        {
            int s1, s2;
            int min = state.Minimum;
            int max = state.Maximum;
            var cards = state.SelectableCards;
            int sum = state.Sum;
            int i = (min <= 1) ? 2 : min;
            while (i <= max && i <= cards.Length)
            {
                var combos = cards.GetCombinations(i);
                foreach (var combo in combos)
                {
                    s1 = 0;
                    s2 = 0;
                    foreach (var card in combo)
                    {
                        s1 += card.OpParam1;
                        s2 += (card.OpParam2 != 0) ? card.OpParam2 : card.OpParam1;
                    }
                    if (s1 == sum || s2 == sum || (!state.MustBeExactValue && (s1 >= sum || s2 >= sum)))
                    {
                        return combo.Select(c => (byte)Array.IndexOf(state.SelectableCards, c)).ToList();
                    }
                }
                i++;
            }
            return null; // Can't find any combination
        }
       
        async Task<IList<CardAttribute>> OnAnnounceAttribute(AnnounceAttributeData state)
        {
            List<CardAttribute> cas = new();
            foreach (CardAttribute ca in System.Enum.GetValues(typeof(CardAttribute)))
                if (state.SelectableOptions.Contains((byte)ca))
                    cas.Add(ca);
            Random rng = new();
            return cas.OrderBy(x => rng.Next()).Take(state.RequiredCount).ToList();
        }
       
        async Task<IList<CardRace>> OnAnnounceRace(AnnounceRaceData state)
        {
            List<CardRace> cas = new();
            foreach (CardRace ca in System.Enum.GetValues(typeof(CardRace)))
                if (state.SelectableOptions.Contains((byte)ca))
                    cas.Add(ca);
            Random rng = new();
            return cas.OrderBy(x => rng.Next()).Take(state.RequiredCount).ToList();
        }
       
        async Task<int> OnAnnounceNumber(AnnounceNumberData state)
        {
            return state.SelectableOptions[0];
        }
        
        async Task<int> OnAnnounceCard(AnnounceCardData state)
        {
            var filters = state.AppliableFilters;
            var cards = await DuelBotWrapper.GetMatchingCards(filters);
            Random rng = new();
            return cards.Cards.ElementAt(rng.Next(0, cards.Cards.Length)).ID;
        }
        
        async Task<int> OnAnnounceCardFilter(AnnounceCardFilterData state)
        {
            var filters = state.AppliableFilters;
            var cards = await DuelBotWrapper.GetMatchingCards(filters);
            Random rng = new();
            return cards.Cards.ElementAt(rng.Next(0, cards.Cards.Length)).ID;
        }
       
        async Task<IList<byte>?> OnSelectUnselectCard(SelectUnselectCardData state)
        {
            List<byte> selection = new();
            for (int i = 0; i < state.Minimum; i++)
                selection[i] = (byte)i;
            return selection;
        }
        
        async Task<IList<short>> OnSelectCounter(SelectCounterData state)
        {
            List<short> resp = new();
            short need = state.RequiredQuantity;
            byte i = 0;
            while (need > 0)
            {
                var amt = state.Counters[i];
                if (amt > need)
                {
                    resp.Add(need);
                    break;
                }
                resp.Add(amt);
                need -= amt;
            }
            return resp;
        }
       
        async Task<int> OnSelectDisfield(SelectDisfieldData state)
        {
            int[] zones = new int[] { Zones.z0, Zones.z1, Zones.z2, Zones.z3, Zones.z4, Zones.z5, Zones.z6 };
            byte seq;
            for (seq = 0; seq < 7; ++seq)
                if (Zones.IsAvailable(zones[seq], state.Filter))
                    break;
            return BitConverter.ToInt32(new byte[] { state.Player, (byte)state.Location, seq, 0 });
        }
       
        async Task<int> OnSelectPlace(SelectPlaceData state)
        {
            int[] zones = new int[] { Zones.z0, Zones.z1, Zones.z2, Zones.z3, Zones.z4, Zones.z5, Zones.z6 };
            byte seq;
            for (seq = 0; seq < 7; ++seq)
                if (Zones.IsAvailable(zones[seq], state.Filter))
                    break;
            return BitConverter.ToInt32(new byte[] { state.Player, (byte)state.Location, seq, 0 });
        }
       
        async Task<bool> OnSelectEffectYn(SelectEffectYnData state)
        {
            return false;
        }
        
        async Task<bool> OnSelectYesNo(SelectYesNoData state)
        {
            return false;
        }
        
        async Task<int> OnSelectOption(SelectOptionData state)
        {
            return 0;
        }
        
        async Task<byte> OnSelectRockPaperScissors()
        {
            return (byte)new Random().Next(1, 4);
        }
       
        async Task<CardPosition> OnSelectPosition(SelectPositionData state)
        {
            return (CardPosition)state.Positions[0];
        }
       
        async Task<IList<byte>> OnSortCard(SortCardData state)
        {
            List<byte> resp = new();
            for (byte i = 0; i < state.SelectableCards.Length; ++i)
                resp.Add(i);
            return resp;
        }
    }
}
