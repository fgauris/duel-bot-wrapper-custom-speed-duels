﻿using BotTest.Decks;
using BotTest.Decks.SpeedDuel.Enums;
using BotTest.Game.AI;
using BotTest.Game.CardActions.Monsters;
using BotTest.Game.CardActions.Monsters.Effects.EffectsActions;
using BotTest.Game.CardActions.Monsters.Effects.Enums;
using BotTest.Game.CardActions.Skill.SkillActions;
using BotTest.Game.CardActions.Spell.Enums;
using BotTest.Game.CardActions.SpellActions.Enums;
using BotTest.Game.CardActions.Trap.Enums;
using BotTest.Game.CardActions.Trap.TrapActions;
using BotTest.Game.Helpers;
using BotTest.Game.Helpers.Enums;
using EnumsNET;
using OmegaBotWrapper;
using OmegaBotWrapper.Enum;
using OmegaBotWrapper.States;
using OmegaBotWrapper.Struct;
using System.Collections;
#pragma warning disable 1998
#pragma warning disable CS8604
namespace BotTest.Game.SpeedDuel.Actions
{
    public class SuperDuckActions : IBotActions
    {
        public string? DeckName { get; set; } = DeckOptions.SuperDuck.AsString(EnumFormat.Description);

        public string? DeckHash { get; set; } = DeckManager.DeckHashes(DeckOptions.SuperDuck);

        public DuelBotWrapper? BotWrapper { get; set; }

        private readonly DefaultExecutor Executor = new();
        private readonly SpellActions SpellExecutor = new();

        private bool IsBotCard = false;

        public void RegisterActions(DuelBotWrapper bot)
        {
            Executor.GetDuelBotWrapper(BotWrapper);
            SpellExecutor.GetDuelBotWrapper(BotWrapper);
            bot.OnBattleCmdAction = OnBattleCmd;
            bot.OnIdleCmdAction = OnIdleCmd;
            bot.OnSelectChainAction = OnSelectChain;
            bot.OnSelectCardAction = OnSelectCard;
            bot.OnSelectTributeAction += OnSelectTribute;
            bot.OnSelectSumAction += OnSelectSum;
            bot.OnAnnounceAttributeAction += OnAnnounceAttribute;
            bot.OnAnnounceRaceAction += OnAnnounceRace;
            bot.OnAnnounceNumberAction += OnAnnounceNumber;
            bot.OnAnnounceCardAction += OnAnnounceCard;
            bot.OnAnnounceCardFilterAction += OnAnnounceCardFilter;
            bot.OnSelectUnselectCardAction += OnSelectUnselectCard;
            bot.OnSelectCounterAction += OnSelectCounter;
            bot.OnSelectDisfieldAction += OnSelectDisfield;
            bot.OnSelectPlaceAction += OnSelectPlace;
            bot.OnSelectEffectYnAction += OnSelectEffectYn;
            bot.OnSelectYesNoAction += OnSelectYesNo;
            bot.OnSelectOptionAction += OnSelectOption;
            bot.OnSelectRockPaperScissors += OnSelectRockPaperScissors;
            bot.OnSelectPositionAction += OnSelectPosition;
            bot.OnSortCardAction += OnSortCard;
        }

        async Task<BattleCmdResponse> OnBattleCmd(BattleCmdData state) => Executor.OnBattleCmd(state);

        async Task<IdleCmdResponse> OnIdleCmd(IdleCmdData state)
        {
            IsBotCard = false;

            //Card Added Order by according your logical Deck
            int[] arrayCardsId = new int[10];
            arrayCardsId[0] = (int)SpellCardID.HammerShot;
            arrayCardsId[1] = (int)SpellCardID.EnchantingFittingRoom;
            arrayCardsId[2] = (int)SpellCardID.AllureOfDarkness;

            var spellActionsResponse = SpellExecutor.SpellCardActivation(state, arrayCardsId);

            if (spellActionsResponse.Item1)
            {

                int cardID = state.ActivableCards[spellActionsResponse.Item2].ID;

                if (cardID == (int)SpellCardID.AllureOfDarkness)
                    IsBotCard = true;

                return state.ActivableCards[spellActionsResponse.Item2].MainActivateFromIndex();
            }

            //If contains any trap setable, set first trap
            if (state.SpellSetableCards.Any(s => s.Type == (byte)CardType.Trap))
                return state.SpellSetableCards.Where(p => p.Type == (byte)CardType.Trap).First().SetSpellTrap();

            if (state.ActivableCards.TryGetByID((int)MonstersEffectsID.BeakerTheMagicalWarrior))
            {
                int index = Array.FindIndex(state.ActivableCards, card => card.ID == (int)MonstersEffectsID.BeakerTheMagicalWarrior && !card.IsDisabled());
                return state.ActivableCards[index].MainActivateFromIndex();
            }

            if (state.SummonableCards.Any())
            {
                var IsJinzo = state.SummonableCards.Where(c => c.IsCode((int)MonstersEffectsID.Jinzo));

                if (IsJinzo.Any())
                {
                    int index = Array.FindIndex(state.SummonableCards, card => card.ID == (int)MonstersEffectsID.Jinzo && !card.IsDisabled());
                    return state.SummonableCards[index].NormalSummon();
                }

                return state.SummonableCards.First().NormalSummon();
            }



            if (state.CanBattlePhase)
            {
                var SkillActionsResponse = SkillActions.SkillCardActivation(state);

                IsBotCard = true;

                if (SkillActionsResponse.Item1)
                    return state.ActivableCards[SkillActionsResponse.Item2].MainActivateFromIndex();

                return MainAction.ToBattlePhase;
            }

            return MainAction.ToEndPhase;
        }

        async Task<int> OnSelectChain(SelectChainData state) => Executor.OnSelectChain(state);

        async Task<IList<byte>?> OnSelectCard(SelectCardData state) => Executor.OnSelectCard(state, IsBotCard);

        async Task<IList<byte>?> OnSelectTribute(SelectTributeData state)
        {
            Random rnd = new();

            int pos = 0;
            List<int> cardTargets = new();

            var AtkMin = state.SelectableCards.Min(m => m.Attack);

            foreach (var card in state.SelectableCards)
            {
                if (card.Attack.Equals(AtkMin))
                {
                    cardTargets.Add(pos);
                    pos++;
                }
                else
                    pos++;
            }

            int index = rnd.Next(cardTargets.Count);

            var cardIndex = cardTargets[index];

            List<byte> resp = new();
            for (byte i = 0; i < state.Minimum; i++)
                resp.Add((byte)cardIndex);
            return resp;
        }

        async Task<IList<byte>?> OnSelectSum(SelectSumData state)
        {
            int s1, s2;
            int min = state.Minimum;
            int max = state.Maximum;
            var cards = state.SelectableCards;
            int sum = state.Sum;
            int i = (min <= 1) ? 2 : min;
            while (i <= max && i <= cards.Length)
            {
                var combos = cards.GetCombinations(i);
                foreach (var combo in combos)
                {
                    s1 = 0;
                    s2 = 0;
                    foreach (var card in combo)
                    {
                        s1 += card.OpParam1;
                        s2 += (card.OpParam2 != 0) ? card.OpParam2 : card.OpParam1;
                    }
                    if (s1 == sum || s2 == sum || (!state.MustBeExactValue && (s1 >= sum || s2 >= sum)))
                    {
                        return combo.Select(c => (byte)Array.IndexOf(state.SelectableCards, c)).ToList();
                    }
                }
                i++;
            }
            return null; // Can't find any combination
        }

        async Task<IList<CardAttribute>> OnAnnounceAttribute(AnnounceAttributeData state)
        {
            List<CardAttribute> cas = new();
            foreach (CardAttribute ca in System.Enum.GetValues(typeof(CardAttribute)))
                if (state.SelectableOptions.Contains((byte)ca))
                    cas.Add(ca);
            Random rng = new();
            return cas.OrderBy(x => rng.Next()).Take(state.RequiredCount).ToList();
        }

        async Task<IList<CardRace>> OnAnnounceRace(AnnounceRaceData state)
        {
            List<CardRace> cas = new();
            foreach (CardRace ca in System.Enum.GetValues(typeof(CardRace)))
                if (state.SelectableOptions.Contains((byte)ca))
                    cas.Add(ca);
            Random rng = new();
            return cas.OrderBy(x => rng.Next()).Take(state.RequiredCount).ToList();
        }

        async Task<int> OnAnnounceNumber(AnnounceNumberData state)
        {
            return state.SelectableOptions[0];
        }

        async Task<int> OnAnnounceCard(AnnounceCardData state)
        {
            var filters = state.AppliableFilters;
            var cards = await DuelBotWrapper.GetMatchingCards(filters);
            Random rng = new();
            return cards.Cards.ElementAt(rng.Next(0, cards.Cards.Length)).ID;
        }

        async Task<int> OnAnnounceCardFilter(AnnounceCardFilterData state)
        {
            var filters = state.AppliableFilters;
            var cards = await DuelBotWrapper.GetMatchingCards(filters);
            Random rng = new();
            return cards.Cards.ElementAt(rng.Next(0, cards.Cards.Length)).ID;
        }

        async Task<IList<byte>?> OnSelectUnselectCard(SelectUnselectCardData state)
        {
            List<byte> selection = new();
            for (int i = 0; i < state.Minimum; i++)
                selection[i] = (byte)i;
            return selection;
        }

        async Task<IList<short>> OnSelectCounter(SelectCounterData state)
        {
            List<short> resp = new();
            short need = state.RequiredQuantity;
            byte i = 0;
            while (need > 0)
            {
                var amt = state.Counters[i];
                if (amt > need)
                {
                    resp.Add(need);
                    break;
                }
                resp.Add(amt);
                need -= amt;
            }
            return resp;
        }

        async Task<int> OnSelectDisfield(SelectDisfieldData state)
        {
            int[] zones = new int[] { Zones.z0, Zones.z1, Zones.z2, Zones.z3, Zones.z4, Zones.z5, Zones.z6 };
            byte seq;
            for (seq = 0; seq < 7; ++seq)
                if (Zones.IsAvailable(zones[seq], state.Filter))
                    break;
            return BitConverter.ToInt32(new byte[] { state.Player, (byte)state.Location, seq, 0 });
        }

        async Task<int> OnSelectPlace(SelectPlaceData state)
        {
            int[] zones = new int[] { Zones.z0, Zones.z1, Zones.z2, Zones.z3, Zones.z4, Zones.z5, Zones.z6 };
            byte seq;
            for (seq = 0; seq < 7; ++seq)
                if (Zones.IsAvailable(zones[seq], state.Filter))
                    break;
            return BitConverter.ToInt32(new byte[] { state.Player, (byte)state.Location, seq, 0 });
        }

        async Task<bool> OnSelectEffectYn(SelectEffectYnData state)
        {
            return true;
        }

        async Task<bool> OnSelectYesNo(SelectYesNoData state)
        {
            return false;
        }

        async Task<int> OnSelectOption(SelectOptionData state)
        {
            return 0;
        }

        async Task<byte> OnSelectRockPaperScissors()
        {
            return (byte)new Random().Next(1, 4);
        }

        async Task<CardPosition> OnSelectPosition(SelectPositionData state)
        {
            return (CardPosition)state.Positions[0];
        }

        async Task<IList<byte>> OnSortCard(SortCardData state)
        {
            List<byte> resp = new();
            for (byte i = 0; i < state.SelectableCards.Length; ++i)
                resp.Add(i);
            return resp;
        }
    }
}
