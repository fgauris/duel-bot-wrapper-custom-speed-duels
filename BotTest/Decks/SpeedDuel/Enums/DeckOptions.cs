﻿using BotTest.Game.AI;
using System.ComponentModel;

namespace BotTest.Decks.SpeedDuel.Enums
{
    
    public enum DeckOptions
    {
        Random,

        [Description("|Speed Duel| Super Duck")]
        SuperDuck,

        [Description("|Speed Duel| Warrior Burn")]
        WarriorBurn,

        [Description("|Speed Duel| |Test| Burn Deck")]
        SpeedDuel_Test_BurnDeck,

        [Description("|Speed Duel| |Test| Pass Turn")]
        SpeedDuel_Test_PassTurn,

        [Description("|Speed Duel| |Test| Beatdown")]
        SpeedDuel_Test_Beatdown,

        [Description("|Speed Duel| |Test| Summon and Pass Turn")]
        SpeedDuel_Test_Summon_PassTurn,

        [Description("|Speed Duel| |Test| Cards Banned")]
        SpeedDuel_Test_Cards_Banned,

        [Description("|Speed Duel| |Test| Destroy and Remove")]
        SpeedDuel_Test_DestroyAndRemove,

        [Description("|Speed Duel| Default")]
        SpeedDuel_Default,

        [Description("|Speed Duel| |Tier 1| Cyber Angels")]
        SpeedDuel_CyberAngels,

        [Description("|Speed Duel| |Tier 1| Joey Beatdown")]
        SpeedDuel_JoeyBeatdown,

        [Description("|Speed Duel| |Tier 1| Trap Monsters")]
        SpeedDuel_TrapMonsters,
    }
}
