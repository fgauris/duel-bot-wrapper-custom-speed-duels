﻿using BotTest.Decks.SpeedDuel.Enums;
using BotTest.Game.SpeedDuel.Actions;
using OmegaBotWrapper;

namespace BotTest.Decks
{
    public class DeckManager
    {
        public static string DeckHashes(DeckOptions deck) => deck switch
        {
            DeckOptions.SuperDuck => "k2Xr3eDIAsIL/quymOhvY37fJMbg93YHKz/LZEYQPhXDwwqSK7G2ZAexQRgkvzP8JzMMH71/kRGEQXoz78xnAbFBapIFDjLc2dDNAsJHE+YwPH/7g8mkMJ4VpGbixIeMLap1DNxXJ7CelBMB2w3SAwA=",
            DeckOptions.WarriorBurn => "k2ZY8F+V5U0/I8P03mIGnZRZrDD8P+4ui8yfIAYQdtHMZvFRsGKczGvDAsLLZn9hvPrtGkPzzHJmEN56agoYRxlnsnhaW7I7r6xggmFPnU1wDLIDAA==",
            DeckOptions.SpeedDuel_Test_BurnDeck => "E2dY3R3PCsN3rksxw/DHzaosMJzC2c8Kw1O/pTDD8ORVfqwwrFZ8iMW/9g0zDIdZW7KDxAA=",
            DeckOptions.SpeedDuel_Test_PassTurn => "k2ZYvDKcFYZTDLoYYbhh2gwGGO5CwlOQ8LovZ5jguKaYBYQtrS3ZLynnMcJw93RxBhgG2QEA",
            DeckOptions.SpeedDuel_Test_Beatdown => "k2ZQs7ZknxT8mAGGy5OC4fivTwEcy63vY4HhrRWbGWB4czEvIwxn3pnP8ui9AsPNtR9ZYfi2hQIDDIPsAgA=",
            DeckOptions.SpeedDuel_Test_Summon_PassTurn => "k2ZQs7ZknxT8mAGGy5OC4fivTwEcy63vY4HhrRWbGWB4czEvIwxn3pnP8ui9AsPNtR9ZYfi2hQIDDIPsAgA=",
            DeckOptions.SpeedDuel_Test_Cards_Banned => "k2eo/bafYeFuNlYYXmo4nxkkxs8ymfH3LEPmxxu0mMuezmUE0TCxNVb9TDDadoYZGIPUxFTsYwDpB6kDmQFig8RgZoDYMD211pbsIDUA",
            DeckOptions.SpeedDuel_Test_DestroyAndRemove => "42G4+u0aQ4tqHYPcqk+sIDaI7t3gyALCIHEYjSwHAA==",

            //Tier 1
            DeckOptions.SpeedDuel_CyberAngels => "k2YvsLZkV0owZ4DhjfvLWEG4r3kWCwwXiKYygrCUdj4zDLeo1jHA8F+vJCYYltVOZu6eLs5wM2EOg0lhPKu3zUnm529/MN3Z0M3irDqBFYa/1EazgtRGGWeygDDIHQA=",
            DeckOptions.SpeedDuel_JoeyBeatdown => "k2BLsrZkD9X7zwTCasWHWBb8V2U5kafKaDRRlwWED4YvZ4VhkDgIh8sHMW89NQWMm2eWg3H3dHGGOddjGf6WWTM8f/uDyaQwntXb5iSzL9B8q212LCCsMecOA0gvSD3IXgA=",
            DeckOptions.SpeedDuel_TrapMonsters => "k2aLsbZkn95bzADDB8OXs4LwiTxVRhgOlw9i3npqChw3zyxnDtX7zwTDasWHWGI0tzLO7ZrFeLalk/mSch4jDM+5HssAw2/6GRkuv9JjAmGQOjapjwwgDHIDAA==",

            _ => "E2WI/LuUMa5RjCFZqIbhiORJBjXtRIbEIF+4GEg+7XM5MwjPZFjMChIHqUsAqoGph6kBqd98hZcJxAYA"
        };

        public static IBotActions GetBotDeck(DeckOptions bot) => bot switch
        {
            DeckOptions.SuperDuck => new SuperDuckActions(),
            DeckOptions.WarriorBurn => new WarriorBurnActions(),
            DeckOptions.SpeedDuel_Test_BurnDeck => new DuelActionsDefault(),
            DeckOptions.SpeedDuel_Test_PassTurn => new PassTurnActions(),
            DeckOptions.SpeedDuel_Test_Beatdown => new BeatdownActions(),
            DeckOptions.SpeedDuel_Test_Summon_PassTurn => new SummonAndPassTurnActions(),
            DeckOptions.SpeedDuel_Test_Cards_Banned => new CardBannedActions(),
            DeckOptions.SpeedDuel_Test_DestroyAndRemove => new DestroyAndRemoveActions(),

            //Tier 1
            DeckOptions.SpeedDuel_CyberAngels => new CyberAngelActions(),
            DeckOptions.SpeedDuel_JoeyBeatdown => new JoeyBeatdownActions(),
            DeckOptions.SpeedDuel_TrapMonsters => new TrapMonsters(),
            //

            _ => new DuelActionsDefault()
        };
    }
}
