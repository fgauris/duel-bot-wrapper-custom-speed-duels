﻿using OmegaBotWrapper;
using BotTest.Decks.SpeedDuel.Enums;
using BotTest.Decks;
using EnumsNET;
using Spectre.Console;
using System.Diagnostics;
#pragma warning disable CS8605

bool endApp = false;

while (!endApp)
{
    //Console.Clear();

    AnsiConsole.Write(
        new FigletText("YGO OMEGA")
            .LeftJustified()
            .Color(Color.Blue));

    AnsiConsole.Write(
        new FigletText("AI_CUSTOM")
            .LeftJustified()
            .Color(Color.Grey));

    AnsiConsole.MarkupLine("\r\n███████╗██████╗ ███████╗███████╗██████╗     ██████╗ ██╗   ██╗███████╗██╗     \r\n██╔════╝██╔══██╗██╔════╝██╔════╝██╔══██╗    ██╔══██╗██║   ██║██╔════╝██║     \r\n███████╗██████╔╝█████╗  █████╗  ██║  ██║    ██║  ██║██║   ██║█████╗  ██║     \r\n╚════██║██╔═══╝ ██╔══╝  ██╔══╝  ██║  ██║    ██║  ██║██║   ██║██╔══╝  ██║     \r\n███████║██║     ███████╗███████╗██████╔╝    ██████╔╝╚██████╔╝███████╗███████╗\r\n╚══════╝╚═╝     ╚══════╝╚══════╝╚═════╝     ╚═════╝  ╚═════╝ ╚══════╝╚══════╝\r\n                                                                             \r\n");

    var choices = Enum.GetValues(typeof(DeckOptions))
    .Cast<DeckOptions>()
    .ToArray();

    var selectedOption = AnsiConsole.Prompt(new SelectionPrompt<DeckOptions>()
        .Title("[green] Select a[/] [underline red bold]BOT[/] \n" +
        "[grey](Move up and down to reveal more bots)[/]")
        .PageSize(15)
        .AddChoices(choices));

    Array deckOptionsList = Enum.GetValues(typeof(DeckOptions));
    Random random = new();

    DeckOptions deck;
    DuelBotWrapper? bot;

    if (selectedOption == DeckOptions.Random)
    {
        deck = (DeckOptions)deckOptionsList.GetValue(random.Next(deckOptionsList.Length));
        bot = new(DeckManager.GetBotDeck(deck));
    }
    else
    {
        deck = (DeckOptions)selectedOption;
        bot = new(DeckManager.GetBotDeck(selectedOption));
    }

    var stopwatch = new Stopwatch();
    stopwatch.Start();

    // Asynchronous
    await AnsiConsole.Status()
        .StartAsync("Thinking...", async ctx =>
        {
            AnsiConsole.MarkupLine("[green bold]-----------------[/]");
            AnsiConsole.MarkupLine("[green bold]  Duel Started...[/]");
            AnsiConsole.MarkupLine("[green bold]-----------------[/]");

            // Update the status and spinner
            ctx.Status("Dueling...");
            ctx.Spinner(Spinner.Known.Clock);
            ctx.SpinnerStyle(Style.Parse("green"));

            if (await bot.Connect())
                await bot.Start();

            stopwatch.Stop();

        });


    //Console.Clear();

    AnsiConsole.MarkupLine("[red bold]-----------------[/]");
    AnsiConsole.MarkupLine("[red bold]Duel Finished![/]");
    AnsiConsole.MarkupLine("[red bold]-----------------[/]");

    AnsiConsole.MarkupLine("[grey bold]-----------------[/]");
    AnsiConsole.MarkupLine("[grey bold]Information: [/]");
    AnsiConsole.MarkupLine("[blue bold]vs: [/] " + deck.AsString(EnumFormat.Description));
    AnsiConsole.MarkupLine($"[yellow bold]Duel Time:[/] {stopwatch.Elapsed.TotalMinutes:F3} min");
    AnsiConsole.MarkupLine($"[yellow bold]Duel Time:[/] {stopwatch.Elapsed.TotalSeconds:F3} sec");
    AnsiConsole.MarkupLine("[grey bold]-----------------[/]");

    var answerReadme = AnsiConsole.Confirm("Do you want [red]CLOSE[/] the app?");

    if (answerReadme) endApp = true;
}
return;